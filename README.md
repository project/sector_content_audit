# Sector Content Audit

Sector Content Audit enables you to audit and review content within your site.

Included is everything you'll need to get going - taxonomies to track audits and progress, text fields to leave review notes or add document notes, an audit date to track and set review dates, as well as an easy to use audit view.

Sector Content Audit is an optional Sector add-on, part of the [Sector ecosystem](https://www.sector.nz/).

## Dependencies

* Sector Content Audit has dependencies to - and requires components from - the Sector Starter Kit

## Features and functionality

Sector Content Audit includes:

- Sector Content audit taxonomy to assign a status during a content audit.
- Sector Content development taxonomy to assign a status during content creation.
- Sector audit field group with audit and progress status, audit date, review and document notes.
- Sector Content Audit views with exposed filters.
- Sample terms in the taxonomies to get you started.

## Installation and configuration

1. Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.
2. For each content type you wish to enable content audit for, visit the content type's *Edit* page, eg `/admin/structure/types/manage/<type>`
   1. Under the "Sector Audit settings" field group, check "Add audit fields to the &lt;type&gt; content type"
3. Edit the Sector Content Audit view and enable/disable appropriate VBO actions to your publishing workflow.

Once that's complete, you should see the Content Audit field group appear when editing nodes of the configured types.

## Included Entities and configuration

### Sector Content audit taxonomy

- `/admin/structure/taxonomy/manage/content_audit/overview`

The Content audit taxonomy allows you to assign a status during a content audit. *Terms are redirected via rabbit hole to a pre-filtered audit view.*

### Sector Content development taxonomy

- `/admin/structure/taxonomy/manage/content_development/overview`

The Content development taxonomy allows you to assign a status during content development. *Terms are redirected via rabbit hole to a pre-filtered audit view.*

### Sector content audit field group

- /admin/structure/types

The field group can be added to all content types. Select the content type and select 'Sector Audit settings' from the settings.

By default the field group includes 5 fields:

- An audit date
- The Content Audit and Content Development term fields
- A plain text review note field
- A formatted text document notes field

### Sector Content Audit view

- `/admin/content/audit`
- `/admin/structure/views/view/sector_content_audit`

The page display follows the structure of the core content with additional columns and filters.

### Sample content

Sample terms in the Content Audit and Content Development vocabularies.
